﻿using BusinessLayer.Services;
using System.Web.Mvc;
using System.Reflection;
using System.Linq;

namespace InterviewProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly string StoriesActionName;
        private readonly string StoriesControllerName;
        private readonly string AboutViewName;
        private readonly string ActionName;
        private readonly string ControllerName;

        private StoryPageContentService _spcService;

        public HomeController()
        {
            StoriesActionName = "Index";
            StoriesControllerName = "Stories";
            AboutViewName = "About";
            ActionName = "action";
            ControllerName = "controller";

            _spcService = new StoryPageContentService();
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            TempData[ActionName] = StoriesActionName;
            TempData[ControllerName] = StoriesControllerName;

            var name = MethodBase.GetCurrentMethod().Name;
            return View(AboutViewName,
                model: _spcService.GetAll().FirstOrDefault(s => s.Title.Contains(name)));
        }
    }
}