﻿using BusinessLayer.Services;
using System.Web.Mvc;

namespace InterviewProject.Controllers
{
    public class StoriesController : Controller
    {
        private StoriesService _storiesService;

        public StoriesController()
        {
            _storiesService = new StoriesService();
        }

        // GET: Stories
        public ActionResult Index()
        {
            return View("GetAllStories", model:_storiesService.GetAll());
        }

        // GET: Stories/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Stories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Stories/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Stories/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Stories/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Stories/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Stories/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
