﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppModel.ViewModels
{
    public class StoryViewModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string PublishedOn { get; set; }
    }
}
