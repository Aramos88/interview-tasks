﻿using System;

namespace AppModel.Entities
{
    public class StoryPageContent
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
