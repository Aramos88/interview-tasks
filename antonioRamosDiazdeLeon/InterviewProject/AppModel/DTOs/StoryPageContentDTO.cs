﻿using System.ComponentModel.DataAnnotations;

namespace AppModel.DTOs
{
    public class StoryPageContentDTO
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "You have exceeded the max amount of characters for a story page content's title.")]
        public string Title { get; set; }
    }
}
