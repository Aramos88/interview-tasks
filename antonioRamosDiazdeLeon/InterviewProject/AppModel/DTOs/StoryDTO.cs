﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppModel.DTOs
{
    public class StoryDTO
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage ="You have exceeded the max amount of characters for a story's title.")]
        public string Title { get; set; }

        [Required]
        [StringLength(int.MaxValue, MinimumLength = 50, ErrorMessage = "You have not reached the minimum length for a story's summary.")]
        public string Summary { get; set; }

        [Required]
        [DataType(DataType.Date, ErrorMessage ="The value provided is not a valid date format.")]
        [DisplayFormat(DataFormatString = "MM/DD/YYYY")]
        public DateTime PublishedOn { get; set; }
    }
}
