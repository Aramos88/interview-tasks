﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Data.Common;
using DAL.Repositories.Base;
using AppModel.Entities;

namespace DAL.Repositories
{
    public class StoryRepository : AdoRepository, IRepository<Story>
    {
        private readonly string CreateSPName;
        private readonly string ReadSPName;
        private readonly string UpdateSPName;
        private readonly string DeleteSPName;

        private readonly string IdParamName;
        private readonly string TitleParamName;
        private readonly string SummaryParamName;
        private readonly string PublishedOnParamName;

        private readonly string EntityIdCollName;
        private readonly string EntityTitleCollName;
        private readonly string EntitySummaryCollName;
        private readonly string EntityPublishedOnCollName;
        private readonly string EntityCreatedOnCollName;
        private readonly string EntityUpdatedOnCollName;

        public StoryRepository()
        {
            CreateSPName = "Stories_Create";
            ReadSPName = "Stories_GetAll";
            UpdateSPName = "Stories_Update";
            DeleteSPName = "Stories_Delete";

            IdParamName = "@Id";
            TitleParamName = "@Title";
            SummaryParamName = "@Summary";
            PublishedOnParamName = "@PublishedOn";

            EntityIdCollName = "pkStories";
            EntityTitleCollName = "sStoryTitle";
            EntitySummaryCollName = "sStorySummary";
            EntityPublishedOnCollName = "dtPublished";
            EntityCreatedOnCollName = "dtCreated";
            EntityUpdatedOnCollName = "dtUpdated";
        }

        public bool AddNew(Story entity)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(CreateSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(TitleParamName, SqlDbType.VarChar).Value = entity.Title;
                command.Parameters.Add(SummaryParamName, SqlDbType.VarChar).Value = entity.Summary;
                command.Parameters.AddWithValue(PublishedOnParamName, SqlDbType.DateTime2).Value = entity.PublishedOn;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (Exception invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }

        public bool Delete(int id)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(DeleteSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(IdParamName, SqlDbType.Int).Value = id;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }

        public List<Story> GetAll()
        {
            var result = new List<Story>();

            try
            {
                var command = new SqlCommand(ReadSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                var adapter = new SqlDataAdapter(command);
                var dataTable = new DataTable();

                Connection.Open();
                adapter.Fill(dataTable);

                foreach (DataRow dr in dataTable.Rows)
                {
                    result.Add(new Story
                    {
                        Id = Convert.ToInt32(dr[EntityIdCollName]),
                        Title = dr[EntityTitleCollName].ToString(),
                        Summary = dr[EntitySummaryCollName].ToString(),
                        PublishedOn = Convert.ToDateTime(dr[EntityPublishedOnCollName]),
                        CreatedOn = Convert.ToDateTime(dr[EntityCreatedOnCollName]),
                        UpdatedOn = Convert.ToDateTime(dr[EntityUpdatedOnCollName])
                    });
                }

                dataTable.Dispose();
                adapter.Dispose();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result;
        }

        public bool Update(Story entity)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(UpdateSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(IdParamName, SqlDbType.Int).Value = entity.Id;
                command.Parameters.AddWithValue(TitleParamName, SqlDbType.VarChar).Value = entity.Title;
                command.Parameters.AddWithValue(SummaryParamName, SqlDbType.VarChar).Value = entity.Summary;
                command.Parameters.AddWithValue(PublishedOnParamName, SqlDbType.DateTime2).Value = entity.PublishedOn;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }

    }
}
