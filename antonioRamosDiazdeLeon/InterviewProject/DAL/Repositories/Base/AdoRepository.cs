﻿using System.Configuration;
using System.Data.SqlClient;

namespace DAL.Repositories.Base
{
    public class AdoRepository
    {
        protected readonly SqlConnection Connection;

        public AdoRepository()
        {
            Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AdoConnectionString"].ConnectionString);
        }
    }
}
