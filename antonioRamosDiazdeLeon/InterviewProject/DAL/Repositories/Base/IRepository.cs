﻿using System.Collections.Generic;

namespace DAL.Repositories.Base
{
    public interface IRepository<T>
    {
        bool AddNew(T entity);
        List<T> GetAll();
        bool Update(T entity);
        bool Delete(int id);
    }
}
