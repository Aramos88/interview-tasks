﻿using AppModel.Entities;
using DAL.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DAL.Repositories
{
    public class StoryPageContentRepository : AdoRepository, IRepository<StoryPageContent>
    {
        private readonly string CreateSPName;
        private readonly string ReadSPName;
        private readonly string UpdateSPName;
        private readonly string DeleteSPName;

        private readonly string IdParamName;
        private readonly string TitleParamName;

        private readonly string EntityIdCollName;
        private readonly string EntityTitleCollName;
        private readonly string EntityCreatedOnCollName;
        private readonly string EntityUpdatedOnCollName;

        public StoryPageContentRepository()
        {
            CreateSPName = "SPC_Create";
            ReadSPName = "SPC_GetAll";
            UpdateSPName = "SPC_Update";
            DeleteSPName = "SPC_Delete";

            IdParamName = "@Id";
            TitleParamName = "@Title";

            EntityIdCollName = "pkStoryPageContent";
            EntityTitleCollName = "sTitle";
            EntityCreatedOnCollName = "dtCreated";
            EntityUpdatedOnCollName = "dtUpdated";
        }

        public bool AddNew(StoryPageContent entity)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(CreateSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(TitleParamName, SqlDbType.VarChar).Value = entity.Title;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }

        public bool Delete(int id)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(DeleteSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(IdParamName, SqlDbType.Int).Value = id;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }

        public List<StoryPageContent> GetAll()
        {
            var result = new List<StoryPageContent>();

            try
            {
                var command = new SqlCommand(ReadSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                var adapter = new SqlDataAdapter(command);
                var dataTable = new DataTable();

                Connection.Open();
                adapter.Fill(dataTable);

                foreach (DataRow dr in dataTable.Rows)
                {
                    result.Add(new StoryPageContent
                    {
                        Id = Convert.ToInt32(dr[EntityIdCollName]),
                        Title = dr[EntityTitleCollName].ToString(),
                        CreatedOn = Convert.ToDateTime(dr[EntityCreatedOnCollName]),
                        UpdatedOn = Convert.ToDateTime(dr[EntityUpdatedOnCollName])
                    });
                }

                command.Dispose();
                dataTable.Dispose();
                adapter.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result;
        }

        public bool Update(StoryPageContent entity)
        {
            int result = -1;

            try
            {
                var command = new SqlCommand(UpdateSPName, Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(IdParamName, SqlDbType.Int).Value = entity.Id;
                command.Parameters.Add(TitleParamName, SqlDbType.VarChar).Value = entity.Title;

                Connection.Open();
                result = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (DbException dbEx)
            {
                Trace.WriteLine(dbEx.Message);
            }
            catch (InvalidOperationException invalidOpEx)
            {
                Trace.WriteLine(invalidOpEx.Message);
            }
            finally
            {
                Connection.Dispose();
            }

            return result >= 1;
        }
    }
}
