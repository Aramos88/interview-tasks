﻿using AppModel.DTOs;
using AppModel.Entities;
using AppModel.ViewModels;
using DAL.Repositories;
using DAL.Repositories.Base;
using System.Collections.Generic;

namespace BusinessLayer.Services
{
    public class StoriesService
    {
        private IRepository<Story> _repo;

        public StoriesService()
        {
            _repo = new StoryRepository();
        }

        public bool CreateNewStory(StoryDTO s)
        {
            return _repo.AddNew(new Story
            {
                Title = s.Title,
                Summary = s.Summary,
                PublishedOn = s.PublishedOn
            });
        }

        public List<StoryViewModel> GetAll()
        {
            return MapMultipleToViewModel(_repo.GetAll());
        }

        public bool UpdateStory(StoryDTO s)
        {
            return _repo.Update(new Story
            {
                Id = s.Id,
                Title = s.Title,
                Summary = s.Summary,
                PublishedOn = s.PublishedOn
            });
        }

        public bool DeleteStory(int id)
        {
            return _repo.Delete(id);
        }

        private List<StoryViewModel> MapMultipleToViewModel(ICollection<Story> stories)
        {
            var result = new List<StoryViewModel>();

            foreach (var s in stories)
            {
                result.Add(new StoryViewModel
                {
                    Title = s.Title,
                    Summary = s.Summary,
                    PublishedOn = s.PublishedOn.ToShortDateString()
                });
            }

            return result;
        }
    }
}
