﻿using AppModel.DTOs;
using AppModel.Entities;
using DAL.Repositories;
using DAL.Repositories.Base;
using System.Collections.Generic;

namespace BusinessLayer.Services
{
    public class StoryPageContentService
    {
        private IRepository<StoryPageContent> _repo;

        public StoryPageContentService()
        {
            _repo = new StoryPageContentRepository();
        }

        public bool CreateNew(StoryPageContentDTO spc)
        {
            return _repo.AddNew(new StoryPageContent
            {
                Title = spc.Title
            });
        }

        public List<StoryPageContentDTO> GetAll()
        {
            return MapMultipleToDTOs(_repo.GetAll());
        }

        public bool UpdateSPC(StoryPageContentDTO spc)
        {
            return _repo.Update(new StoryPageContent
            {
                Id = spc.Id,
                Title = spc.Title
            });
        }

        public bool DeleteSPC(int id)
        {
            return _repo.Delete(id);
        }

        private List<StoryPageContentDTO> MapMultipleToDTOs(ICollection<StoryPageContent> spcs)
        {
            var result = new List<StoryPageContentDTO>();

            foreach (var s in spcs)
            {
                result.Add(new StoryPageContentDTO
                {
                    Id = s.Id,
                    Title = s.Title
                });
            }

            return result;
        }
    }
}
