USE [master]
GO
/****** Object:  Database [InterviewProject]    Script Date: 09/07/2011 15:35:10 ******/
CREATE DATABASE [InterviewProject]
GO
ALTER DATABASE [InterviewProject] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [InterviewProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [InterviewProject] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [InterviewProject] SET ANSI_NULLS OFF
GO
ALTER DATABASE [InterviewProject] SET ANSI_PADDING OFF
GO
ALTER DATABASE [InterviewProject] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [InterviewProject] SET ARITHABORT OFF
GO
ALTER DATABASE [InterviewProject] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [InterviewProject] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [InterviewProject] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [InterviewProject] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [InterviewProject] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [InterviewProject] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [InterviewProject] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [InterviewProject] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [InterviewProject] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [InterviewProject] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [InterviewProject] SET  DISABLE_BROKER
GO
ALTER DATABASE [InterviewProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [InterviewProject] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [InterviewProject] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [InterviewProject] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [InterviewProject] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [InterviewProject] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [InterviewProject] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [InterviewProject] SET  READ_WRITE
GO
ALTER DATABASE [InterviewProject] SET RECOVERY FULL
GO
ALTER DATABASE [InterviewProject] SET  MULTI_USER
GO
ALTER DATABASE [InterviewProject] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [InterviewProject] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'InterviewProject', N'ON'
GO

USE [InterviewProject]
GO

/****** Object:  Table [dbo].[MainPageContent]    Script Date: 09/07/2011 15:35:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MainPageContent](
	[pkMainPageContent] [int] IDENTITY(1,1) NOT NULL,
	[sTitle] [varchar](500) NOT NULL,
/*			[text] data type deprecated starting from SQL Server 2008				*/
	[sStory] [varchar](max) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[dtUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_MainPageContent] PRIMARY KEY CLUSTERED 
(
	[pkMainPageContent] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StoryPageContent]    Script Date: 09/07/2011 15:35:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StoryPageContent](
	[pkStoryPageContent] [int] IDENTITY(1,1) NOT NULL,
	[sTitle] [varchar](500) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[dtUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_StoryPageContent] PRIMARY KEY CLUSTERED 
(
	[pkStoryPageContent] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stories]    Script Date: 09/07/2011 15:35:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stories](
	[pkStories] [int] IDENTITY(1,1) NOT NULL,
	[sStoryTitle] [varchar](500) NOT NULL,
/*			[text] data type deprecated starting from SQL Server 2008				*/
	[sStorySummary] [varchar](max) NOT NULL,
	[dtPublished] [datetime] NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[dtUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_Stories] PRIMARY KEY CLUSTERED 
(
	[pkStories] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SaveMainPageContent]    Script Date: 09/07/2011 15:35:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SaveMainPageContent]
@pkMainPageContent int, 
@sTitle varchar(500) = null, 
@sStory text = null

as
if exists (select top 1 pkMainPageContent from MainPageContent where pkMainPageContent = @pkMainPageContent)
begin
	update MainPageContent set 
		sTitle = isnull(@sTitle, sTitle), 
		sStory = isnull(@sStory, sStory),
		dtUpdated = getdate()
	where pkMainPageContent = @pkMainPageContent
	
	select @pkMainPageContent as pkMainPageContent
end
else
begin
	insert into MainPageContent (sTitle, sStory)
	values (isnull(@sTitle, ''), 
		isnull(@sStory, ''))
	
	select @@identity as pkMainPageContent
end
GO
/****** Object:  StoredProcedure [dbo].[GetMainPageContent]    Script Date: 09/07/2011 15:35:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetMainPageContent]

as
select top 1 pkMainPageContent, sTitle, sStory, dtCreated, dtUpdated
from MainPageContent
order by dtUpdated desc
GO
/****** Object:  StoredProcedure [dbo].[DeleteMainPageContent]    Script Date: 09/07/2011 15:35:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[DeleteMainPageContent]
@pkMainPageContent int

as
set nocount on
delete from MainPageContent
where pkMainPageContent = @pkMainPageContent
GO
/****** Object:  Default [DF_MainPageContent_sTitle]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[MainPageContent] ADD  CONSTRAINT [DF_MainPageContent_sTitle]  DEFAULT ('') FOR [sTitle]
GO
/****** Object:  Default [DF_MainPageContent_sStory]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[MainPageContent] ADD  CONSTRAINT [DF_MainPageContent_sStory]  DEFAULT ('') FOR [sStory]
GO
/****** Object:  Default [DF_Table_1_dtInserted]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[MainPageContent] ADD  CONSTRAINT [DF_Table_1_dtInserted]  DEFAULT (getdate()) FOR [dtCreated]
GO
/****** Object:  Default [DF_MainPageContent_dtUpdated]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[MainPageContent] ADD  CONSTRAINT [DF_MainPageContent_dtUpdated]  DEFAULT (getdate()) FOR [dtUpdated]
GO
/****** Object:  Default [DF_StoryPageContent_sTitle]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[StoryPageContent] ADD  CONSTRAINT [DF_StoryPageContent_sTitle]  DEFAULT ('') FOR [sTitle]
GO
/****** Object:  Default [DF_StoryPageContent_dtCreated]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[StoryPageContent] ADD  CONSTRAINT [DF_StoryPageContent_dtCreated]  DEFAULT (getdate()) FOR [dtCreated]
GO
/****** Object:  Default [DF_StoryPageContent_dtUpdated]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[StoryPageContent] ADD  CONSTRAINT [DF_StoryPageContent_dtUpdated]  DEFAULT (getdate()) FOR [dtUpdated]
GO
/****** Object:  Default [DF_Stories_sStoryTitle]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[Stories] ADD  CONSTRAINT [DF_Stories_sStoryTitle]  DEFAULT ('') FOR [sStoryTitle]
GO
/****** Object:  Default [DF_Stories_sStorySummary]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[Stories] ADD  CONSTRAINT [DF_Stories_sStorySummary]  DEFAULT ('') FOR [sStorySummary]
GO
/****** Object:  Default [DF_Stories_dtPublished]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[Stories] ADD  CONSTRAINT [DF_Stories_dtPublished]  DEFAULT (getdate()) FOR [dtPublished]
GO
/****** Object:  Default [DF_Stories_dtCreated]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[Stories] ADD  CONSTRAINT [DF_Stories_dtCreated]  DEFAULT (getdate()) FOR [dtCreated]
GO
/****** Object:  Default [DF_Stories_dtUpdated]    Script Date: 09/07/2011 15:35:14 ******/
ALTER TABLE [dbo].[Stories] ADD  CONSTRAINT [DF_Stories_dtUpdated]  DEFAULT (getdate()) FOR [dtUpdated]
GO
