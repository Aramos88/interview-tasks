USE [InterviewProject]
GO

SET IDENTITY_INSERT MainPageContent ON
INSERT INTO dbo.MainPageContent(pkMainPageContent, sTitle, sStory, dtCreated, dtUpdated)
VALUES (1, 'Obama jobs plan could complicate deficit panel work', 'US President Barack Obama delivers remarks at the University of Richmond 
on the bipartisan proposals to grow the economy and create jobs as part of the American Jobs Act in Richmond, Virginia September 9, 2011.',
GETDATE(), GETDATE()), 
(2, 'Biden: Bin Laden wanted 9/11 anniversary attack', 'Vice President Biden said today that evidence found at Osama bin Laden''s compound 
in Pakistan indicated that he wanted an attack on the US during this weekend''s 10th anniversary of the 9/11 terrorist strikes.',
GETDATE(), GETDATE()),
(3, 'Ali Lohan''s New Face Explained', 'After a new photo of Ali Lohan set off an Internet storm about her dramatic look,
 her rep says Lindsay Lohan''s younger sister, who was recently signed to a modeling agency, did not go under the knife.',
GETDATE(), GETDATE());
SET IDENTITY_INSERT MainPageContent OFF

SET IDENTITY_INSERT Stories ON
INSERT INTO dbo.Stories(pkStories, sStoryTitle, sStorySummary, dtPublished, dtCreated, dtUpdated)
VALUES(1, 'Patient privacy in spotlight after hospital records posted online',
'(CBS/AP) A patient privacy breach at a prominent California hospital serves as a reminder that records aren''t as secure as patients may hope.',
'20110909',GETDATE(), GETDATE()),
(2, 'Sprint iPhone, unlimited data? Not for long',
'If Sprint Nextel sells the next iPhone with an unlimited plan, snap it up while you can, because it may end up as a limited time offer.',
'20110908',GETDATE(), GETDATE()),
(3, 'Interpol issues Red Notice warrants for Gadhafi',
'By the CNN Wire Staff A picture of Moammar Gadhafi taken in 2009. Interpol has issued Red Notice warrants for the fallen Libyan leader.',
'20110907',GETDATE(), GETDATE()),
(4, 'Stocks, Euro Tumble on Greece Debt Concern',
'US President Barack Obama, House Majority Leader Eric Cantor, a Virginia Republican, and Thomas Barrack, chairman of Colony Capital LLC, offer their views on the ...',
'20110906',GETDATE(), GETDATE()),
(5, 'Syrian claims soldiers are buried',
'Syrian protesters have taken to the streets in their thousands following Friday prayers, calling for international protection from the security forces.',
'20110906',GETDATE(), GETDATE()),
(6, 'Israel Seeks to Calm Tensions With Turkey',
'Israel was wrestling on Friday with growing tensions with Turkey after the Turkish prime minister threatened to use his navy to accompany aid flotillas
to Gaza and to challenge Israel''s plans for gas exploration and export ...',
'20110905',GETDATE(), GETDATE());
SET IDENTITY_INSERT Stories OFF

SET IDENTITY_INSERT StoryPageContent ON
INSERT INTO dbo.StoryPageContent(pkStoryPageContent, sTitle, dtCreated, dtUpdated)
VALUES(1, 'This is the About Us Page', GETDATE(), GETDATE()),
(2, 'This is the Home Page', GETDATE(), GETDATE()),
(3, 'This is the Contact Us Page', GETDATE(), GETDATE()),
(4, 'This is the Privacy Policy Page', GETDATE(), GETDATE());
SET IDENTITY_INSERT StoryPageContent OFF