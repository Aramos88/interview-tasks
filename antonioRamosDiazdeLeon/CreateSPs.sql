USE [InterviewProject]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Stories_Create
	@Title varchar(500),
	@Summary varchar(max),
	@PublishedOn datetime2
AS
BEGIN
	SET NOCOUNT OFF;
	INSERT INTO Stories VALUES(@Title, @Summary, @PublishedOn, GETDATE(), GETDATE());
END
GO

CREATE PROCEDURE Stories_GetAll
AS
BEGIN
	SET NOCOUNT OFF;
	SELECT * FROM Stories
END
GO

CREATE PROCEDURE Stories_Update
	@Id int,
	@Title varchar(500), 
	@Summary varchar(max),
	@PublishedOn datetime2
AS
BEGIN
	SET NOCOUNT OFF;
	UPDATE Stories
	SET sStoryTitle = @Title,
		sStorySummary = @Summary,
		dtPublished = @PublishedOn,
		dtUpdated = GETDATE()
		WHERE pkStories = @Id
END
GO

CREATE PROCEDURE Stories_Delete
	@Id int
AS
BEGIN
	SET NOCOUNT OFF;
	DELETE FROM Stories WHERE pkStories = @Id
END
GO


--StoryPageContent	Procedures

CREATE PROCEDURE SPC_Create
	@Title varchar(500)
AS
BEGIN
	SET NOCOUNT OFF;
	INSERT INTO StoryPageContent VALUES(@Title, GETDATE(), GETDATE());
END
GO

CREATE PROCEDURE SPC_GetAll
AS
BEGIN
	SET NOCOUNT OFF;
	SELECT * FROM StoryPageContent
END
GO

CREATE PROCEDURE SPC_Update
	@Id int,
	@Title varchar(500)
AS
BEGIN
	SET NOCOUNT OFF;
	UPDATE StoryPageContent
	SET sTitle = @Title
	WHERE pkStoryPageContent = @Id
END
GO

CREATE PROCEDURE SPC_Delete
	@Id int
AS
BEGIN
	SET NOCOUNT OFF;
	DELETE FROM StoryPageContent WHERE pkStoryPageContent = @Id
END
GO