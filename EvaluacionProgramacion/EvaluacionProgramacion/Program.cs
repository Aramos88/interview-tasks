﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EvaluacionProgramacion
{
    class Program
    {
        static void Main(string[] args)
        {
            PingPongMethod();

            Console.WriteLine("\n\nPlease write a phrase or thought. The program will invert a word within the sentence.");
            InvertSingleWordInPhrase(Console.ReadLine());

            Console.WriteLine("\n\nThe biggest sum for whole numbers in the array is: "
                + GetBiggestSumForArrayElements(new[] { -10, -2, 3, -2, 0, 5, -15 }));

            try
            {
                Console.WriteLine("\n\nPlease enter an Integer Number to count the active/set bits:");
                Console.WriteLine("The bit count for the number you input is: " +
                    CountSetBits(Convert.ToInt32(Console.ReadLine())));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Most likely the input you provided CANNOT be converted to System.Int32");
                Console.WriteLine("Thrown Exception: "+ex.Message);
            }

            Console.WriteLine("\n\nPress any key to return to Visual Studio...");
            Console.Read();
        }

        private static int CountSetBits(int n)
        {
            return Convert.ToString(n, 2).ToCharArray().Count(bit => bit == '1');
        }

        private static int GetBiggestSumForArrayElements(int[] array)
        {
            var sums = new List<int>();
            int index = 0;

            Array.ForEach(array, (n) =>
            { 
                if (n <= 0)
                {
                    index++;
                    return;
                }

                for (int i = 1; i < array.Length - index; i++)
                {
                    if (array[index + i] <= 0)
                    {
                        continue;
                    }

                    sums.Add(n + array[index + i]);
                }

                index++;

            });

            return sums.Max();
        }

        private static void PingPongMethod()
        {
            for (int i = 0; i < 30; i++)
            {
                if (i % 5 == 0 && i != 0)
                {
                    Console.WriteLine("Ping-Pong");
                    continue;
                }
                else if (i % 2 == 0)
                {
                    Console.WriteLine("Ping");
                    continue;
                }
                else
                {
                    Console.WriteLine("Pong");
                    continue;
                }
            }
        }

        private static void InvertSingleWordInPhrase(string phrase)
        {
            var strArray = phrase.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            var index = new Random().Next(strArray.Length);
            strArray[index] = string.Concat(strArray[index].Reverse());

            Console.WriteLine(string.Join(" ", strArray));
        }
    }
}
